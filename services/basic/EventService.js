// Responsibility: Initialize and share emitter

const { EventEmitter } = require('events');
const eventEmitter = new EventEmitter();

function getEmitter() {
    return eventEmitter;
}

exports.getEmitter = getEmitter;