// Responsibility: logging

const eventEmitter = require('./EventService.js').getEmitter();
const fs = require('fs');

module.exports = class LogService {
    constructor() {
        // Check if logs folder exists, otherwise create it
        if (!fs.existsSync('./logs')) fs.mkdirSync('./logs');

        // Event listener for logging
        eventEmitter.on('log', async (message) => {
            await this.write(message);
        });

        // Event listener for warning
        eventEmitter.on('log-warning', async (message) => {
            await this.write(`Warning: ${message}`);
        });

        // Event listener for error
        eventEmitter.on('log-error', async (message) => {
            await this.write(`${message}`);
        });

        let startupMessage = `\r\n-------------\r\nStarted LogService succesfully.`
        this.write(startupMessage);
    }
    async write(message) {
        // Format Filename
        // Get Date
        let date = new Date();
        // Add offset
        let timezoneOffset = date.getTimezoneOffset() * -1;
        let adjustedTime = date.getTime() + (timezoneOffset * 60 * 1000);

        // New Time
        let newDate = new Date(adjustedTime);
        let currentDate = `${newDate.getUTCFullYear()}-${newDate.getUTCMonth() + 1}-${newDate.getUTCDate()}`;

        let logDateFormat = newDate.toISOString();
        logDateFormat = logDateFormat.replace("T", " ");
        logDateFormat = logDateFormat.replace("Z", "");
        logDateFormat = logDateFormat.replaceAll("-", "/");
        logDateFormat = logDateFormat.slice(0, logDateFormat.length - 4);

        // Format path
        let fileName = `${currentDate}.txt`;
        let path = `./logs/${fileName}`;

        let messageWithTime = `\r\n${logDateFormat} ${message} `
        // Write to console
        process.stdout.write(messageWithTime);

        // Write to todays log file
        fs.appendFile(path, messageWithTime, err => {
            if (err) {
                console.log("Error writing to log file. ERROR: " + err);
                return;
            }
        });
    }
}