// Responsibility: communicating with IRC network

const eventEmitter = require('../basic/EventService').getEmitter();

module.exports = class NetworkCommunicatorService {
    constructor() {
        this.socket = null;
        eventEmitter.on('network-socket', (socket) => {
            this.socket = socket;
        });

        eventEmitter.on('network-data-send', (data) => {
            if (this.socket === null) {
                console.log(`While trying to send data ${data}, found no socket!`);
                return;
            }
            if (process.env.DEBUG) {
                eventEmitter.emit('log', `Sending data: ${data}`);
            }
            this.socket.write(`${data}\n`)
        });
    }
}