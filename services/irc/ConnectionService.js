// Responsibility: maintaining irc connection

const eventEmitter = require('../basic/EventService.js').getEmitter();
const net = require('net');

const State = {
    CONNECTING: 'connecting',
    CONNECTED: 'connected',
    DISCONNECTED: 'disconnected',
    TIMEOUT: 'timeout',
    ERROR: 'error',
    CLOSED: 'closed'
}
Object.freeze(State);

module.exports = class ConnectionService {
    constructor(address = 'irc.quakenet.org', port = 6667) {
        eventEmitter.emit('log', 'Connection Service started');
        this.socket = new net.Socket();
        this.address = address;
        this.port = port;
        this.retryConnectionFunction = null;

        this.connect();

        // Socket connection succesfull
        this.socket.on('connect', async () => {
            this.updateState(State.CONNECTING);
        });

        // Socket is ready to be used, trigged after 'connect'
        this.socket.on('ready', async () => {
            this.updateState(State.CONNECTED);
        });

        // Data received
        this.socket.on('data', async (data) => {
            data = data.toString();
            data = data.trim();
            eventEmitter.emit('network-data-received', data);
            // Debug print & log
            if (process.env.DEBUG) {
                eventEmitter.emit('log', data);
            }
        });

        // Error occured, close-event will follow
        this.socket.on('error', async (error) => {
            this.updateState(State.ERROR, error);
        });

        // Timeout, socket must be closed manually.        
        this.socket.on('timeout', async () => {
            this.updateState(State.TIMEOUT);
        });

        // Socket fully closed
        this.socket.on('close', async () => {
            this.updateState(State.CLOSED);
        });

        // Other end sends FIN packet, no longer readable
        this.socket.on('end', async () => {
            this.updateState(State.DISCONNECTED);
        });
    }

    async updateState(state, message) {
        if (this.state == state) return;
        eventEmitter.emit('log', `IRC connection state changed from ${this.state} to ${state}`);
        this.state = state;
        switch (this.state) {
            case State.CONNECTING:
                eventEmitter.emit('log', `Trying to connect to ${this.address}:${this.port}`);
                break;
            case State.CONNECTED:
                if (this.retryConnectionFunction != null) {
                    clearInterval(this.retryConnectionFunction);
                    this.retryConnectionFunction = null;
                }
                eventEmitter.emit('log', `Succesfully connected to ${this.address}:${this.port}`);
                eventEmitter.emit('network-socket', this.socket);
                eventEmitter.emit('irc-connection', 'authentication-required');
                break;
            case State.DISCONNECTED:
                this.socket.end();
                this.retryConnectionFunction = setInterval(this.connect, 120000);
                break;
            case State.TIMEOUT:
                this.socket.end();
                this.retryConnectionFunction = setInterval(this.connect, 120000);
                break;
            case State.ERROR:
                eventEmitter.emit('log-error', message);
                eventEmitter.emit('app-close', true);
                break;
            case State.CLOSED:
                eventEmitter.emit('log', `IRC connection closed. Ending program.`);
                this.socket.destroy();
                break;
            default:
                eventEmitter.emit('log-error', `Trying to change IRC connection state to an unsupported one.`);
                this.socket.destroy();
                break;
        }
    }

    async connect() {
        try {
            this.updateState(State.CONNECTING);
            this.socket = net.createConnection(this.port, this.address);
        } catch (error) {
            this.updateState(State.ERROR, error);
        }
    }
}