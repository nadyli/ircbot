// Responsibility: parse raw message to object form with relevant data

const eventEmitter = require('../basic/EventService.js').getEmitter();
const Message = require('../../models/message');

module.exports = class DataParserService {
    constructor() {
        eventEmitter.on('network-data-received', (data) => {
            let messageArray = data.split(" ");

            if (messageArray.length >= 4) {
                let userArray = messageArray[0].slice(1).split("!");
                let sender = userArray[0];
                let address = userArray[1];
                let target = messageArray[2];
                let message = messageArray.slice(3).join(" ");
                message = message.slice(1);

                if (!sender || !address || !target || !message) return;
                if (sender.includes('quakenet') || target.includes('quakenet') || target.includes(':') || sender.includes('Q')) return;

                let msg = new Message(target, sender, address, message);

                eventEmitter.emit('parsed-message', msg);
            }
        });

        eventEmitter.on('irc-message-send', data => {
            eventEmitter.emit('network-data-send', `PRIVMSG ${data.target} :${data.message}`);
        });
    }
}