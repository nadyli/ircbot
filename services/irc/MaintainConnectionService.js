// Responsibility: handle ping-pong

const eventEmitter = require('../basic/EventService.js').getEmitter();
const json = require('../utility/json');

module.exports = class MaintainConnectionService {
    constructor() {
        // PING :3860498158
        eventEmitter.on('network-data-received', (data) => {
            if (data.includes("PING :")) {
                let dataArray = data.split(" ");
                let indexOfPing = dataArray.findIndex(element => element.includes("PING"));
                let pingID = dataArray[indexOfPing + 1];
                eventEmitter.emit('network-data-send', `PONG ${pingID}`);
            }
            // Nickname already in use - :underworld1.no.quakenet.org 433 * iskuri :Nickname is already in use.
            if (data.includes("Nickname is already in use.")) {
                eventEmitter.emit('log', 'Nickname is already in use.');
                eventEmitter.emit('irc-connection', 'nickname-already-in-use');
            }
            // Succesfully connected to server - :euroserv.fr.quakenet.org 376 juhanrandombott :End of /MOTD command.
            if (data.includes("/MOTD")) {
                eventEmitter.emit('irc-connection', 'successfully-authenticated');
            }
            if (data.includes("You are now logged in")) {
                eventEmitter.emit('log', 'Succesfully authenticated to QuakeNet. Trying to log in with Q.');
                eventEmitter.emit('irc-connection', 'successfully-logged-in');
            }
            if (data.includes("is now your hidden host")) {
                eventEmitter.emit('log', 'Succesfully hid hostname.');
                eventEmitter.emit('irc-connection', 'you-are-now-hidden');
            }
        });
    }
}