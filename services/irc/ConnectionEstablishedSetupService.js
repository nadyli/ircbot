// Responsibility: handle initial connection setup

const eventEmitter = require('../basic/EventService.js').getEmitter();
const json = require('../utility/json');

module.exports = class ConnectionEstablishedSetupService {
    constructor() {
        eventEmitter.emit('log', 'Connection Established Setup Service started');
        eventEmitter.on('irc-connection', async (data) => {
            if (data.includes('successfully-authenticated')) {
                eventEmitter.emit('network-data-send', `PRIVMSG Q@CServe.quakenet.org :AUTH ${process.env.QAUTH} ${process.env.QPASSWORD}`);
                if (process.env.QHIDEHOST = true) {
                    await this.hideHost()
                }
                setTimeout(async () => {
                    await this.joinChannels();
                }, 5000);
            }
        });
    }

    async joinChannels() {
        eventEmitter.emit('log', 'Joining QuakeNet chat channels.');
        let configuration = await json.load(['config', 'currentConfiguration.json'], true);
        let channels = configuration.enviroments[process.env.NODE_ENV].channels
        if (channels.includes(',')) {
            let separatedChannels = channels.split(",");
            separatedChannels.forEach(element => {
                element = element.trim();
                eventEmitter.emit('channel-join', element);
            });
        } else {
            eventEmitter.emit('channel-join', channels);
        }
    }

    async hideHost() {
        eventEmitter.emit('log', 'Q Hide Host has been set to true. Trying to hide hostname.');
        let configuration = await json.load(['config', 'currentConfiguration.json'], true);
        eventEmitter.emit('network-data-send', `MODE ${configuration.current_bot_name} +x`);
    }
}