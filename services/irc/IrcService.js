// Responsibility: maintaining different irc services

const eventEmitter = require('../basic/EventService').getEmitter();
const ConnectionEstablishedSetupService = require('./ConnectionEstablishedSetupService');
const ChannelService = require('./ChannelService');
const ConnectionService = require('./ConnectionService');
const AuthenticationService = require('./AuthenticationService');
const NetworkCommunicatorService = require('./NetworkCommunicatorService');
const MaintainConnectionService = require('./MaintainConnectionService');
const DataParserService = require('./DataParserService');

module.exports = class IrcService {
    constructor() {
        new ConnectionEstablishedSetupService();
        new ChannelService();
        new MaintainConnectionService();
        new NetworkCommunicatorService();
        new AuthenticationService();
        new ConnectionService();
        new DataParserService();
    }
}
