// Responsibility: handle channels

const eventEmitter = require('../basic/EventService.js').getEmitter();

module.exports = class ChannelService {
    constructor() {
        eventEmitter.on('channel-join', channel => {
            eventEmitter.emit('network-data-send', `JOIN #${channel}`);
        });
        eventEmitter.on('channel-leave', channel => {
            eventEmitter.emit('network-data-send', `PART #${channel}`);
        });
        eventEmitter.on('channel-kick', (channel, target) => {
            eventEmitter.emit('network-data-send', `KICK #${channel} ${target}`);
        });
        eventEmitter.on('channel-ban', (channel, target) => {
            eventEmitter.emit('network-data-send', `MODE #${channel} +b ${target}`);
        });
        eventEmitter.on('channel-kickban', (channel, target) => {
            eventEmitter.emit('network-data-send', `MODE #${channel} +b ${target}`);
            eventEmitter.emit('network-data-send', `KICK #${channel} ${target}`);
        });
    }
}