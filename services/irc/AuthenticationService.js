// Responsibility: authenticating to the network and q

const eventEmitter = require('../basic/EventService').getEmitter();
const json = require('../utility/json');

module.exports = class AuthenticationService {
    constructor() {
        eventEmitter.emit('log', 'Authentication Service started');
        this.iterator = 0;
        eventEmitter.on('irc-connection', async (state) => {
            let configuration = await json.load(['config', 'currentConfiguration.json'], true);

            switch (state) {
                case "authentication-required":
                    await this.authenticate(configuration.enviroments[process.env.NODE_ENV].bot_name);
                    break;
                case 'nickname-already-in-use':
                    this.iterator++;
                    await this.authenticate(configuration.enviroments[process.env.NODE_ENV].bot_name + this.iterator);
                    break;
                case 'successfully-authenticated':
                    if (this.iterator > 0) {
                        configuration.current_bot_name = configuration.enviroments[process.env.NODE_ENV].bot_name + this.iterator;
                    } else {
                        configuration.current_bot_name = configuration.enviroments[process.env.NODE_ENV].bot_name;
                    }
                    await json.save(['config', 'currentConfiguration.json'], configuration);
                    this.iterator = 0;
                    break;
                default:
                    break;
            }
        });
    }

    async authenticate(username) {
        eventEmitter.emit('log', 'Sending authentication data for QuakeNet.');
        // Sending authentication to ident server
        // NICK <nickname>
        eventEmitter.emit('network-data-send', `NICK ${username}`);
        // USER <user> <mode> <unused> <realname>
        // Real name may contain spaces, but in that case must be prefixed by :
        eventEmitter.emit('network-data-send', `USER ${username} <mode> <unused> :Iskurin ikioma botti`);
    }
}