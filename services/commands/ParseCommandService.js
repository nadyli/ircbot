// Responsibility: launch message for commands

const eventEmitter = require('../basic/EventService').getEmitter();
const json = require('../utility/json');
const Message = require('../../models/message');

module.exports = class ParseCommandService {
    constructor() {
        eventEmitter.on('parsed-message', data => {
            this.parseCommand(data);
        });
    }

    async parseCommand(data) {
        let hasCommandPrefix = await this.hasCommandPrefix(data.message);
        if (!hasCommandPrefix) return;

        let textCommand = await this.hasTextCommand(hasCommandPrefix);
        if (textCommand) {
            let msg = new Message(data.target, 'me', 'my@address', textCommand.response);
            eventEmitter.emit('irc-message-send', msg);
            return;
        }

        let reservedCommand = await this.hasReservedCommand(hasCommandPrefix);
        if (reservedCommand) {
            eventEmitter.emit('reserved-command', data)
            return;
        }
    }

    async hasCommandPrefix(message) {
        let splitMessage = message.split(" ");
        if (splitMessage[0].charAt(0).includes("!")) {
            return splitMessage[0];
        } else {
            return false;
        }
    }

    /*
   Problem: 
   No check if commands.json is found?
   Do we just trust json.load function?
    */
    async hasTextCommand(command) {
        let commands = await json.load(['config', 'currentCommands.json']);
        for await (const c of commands) {
            if (c.command == command) {
                return c;
            }
        }
        return false;
    }

    async hasReservedCommand(command) {
        let content = await json.load(['config', 'reservedCommands.json']);
        for await (const c of content.commands) {
            if (c === command) {
                return true;
            }
        }
        return false;
    }
}