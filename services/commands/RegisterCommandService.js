// Responsibility: register new commands

const eventEmitter = require('../basic/EventService').getEmitter();
const json = require('../utility/json');
const Message = require('../../models/message');

module.exports = class RegisterCommandService {
    constructor() {
        eventEmitter.on('command-add', (data) => {
            this.registerCommand(data);
        });

        eventEmitter.on('command-remove', (command) => {
            this.removeCommand(command);
        });
    }

    async registerCommand(data) {
        console.log(data)
        if (!data.command.charAt(0).includes("!")) {
            let msg = new Message(data.channel, 'me', 'my@address', `Muista lisätä ! komennon alkuun.`);
            eventEmitter.emit('irc-message-send', msg);
            return;
        }
        let exists = await this.checkIfCommandExists(data.command);
        if (exists) {
            let msg = new Message(data.channel, 'me', 'my@address', `${data.command} komento löytyy jo.`);
            eventEmitter.emit('irc-message-send', msg);
            return;
        }
        let commands = await json.load(['config', 'currentCommands.json']);
        commands.push({ command: data.command, response: data.response });
        await json.save(['config', 'currentCommands.json'], commands);
        let msg = new Message(data.channel, 'me', 'my@address', `${data.command} komento lisätty onnistuneesti.`);
        eventEmitter.emit('irc-message-send', msg);
    }

    async removeCommand(data) {
        let exists = await this.checkIfCommandExists(data.command);
        if (!exists) {
            let msg = new Message(data.channel, 'me', 'my@address', `${data.command} komentoa ei löydetty.`);
            eventEmitter.emit('irc-message-send', msg);
            return;
        }
        let commands = await json.load(['config', 'currentCommands.json']);
        let index = commands.findIndex(element => element.command == data.command);
        if (index === -1) return;
        commands.splice(index, 1)
        await json.save(['config', 'currentCommands.json'], commands);
        let msg = new Message(data.channel, 'me', 'my@address', `${data.command} komento poistettu onnistuneesti.`);
        eventEmitter.emit('irc-message-send', msg);
    }

    async checkIfCommandExists(command) {
        let commands = await json.load(['config', 'currentCommands.json']);
        for await (const c of commands) {
            if (c.command == command) {
                return true;
            }
        }
        let content = await json.load(['config', 'reservedCommands.json']);
        for await (const c of content.commands) {
            if (c === command) {
                return true;
            }
        }
        return false;
    }
}
