// Responsibility: detect reserved command
const eventEmitter = require('../basic/EventService').getEmitter();
const json = require('../utility/json');
const Message = require('../../models/message');

module.exports = class DetectReservedCommand {
    constructor() {
        eventEmitter.on('reserved-command', data => {
            let message = data.message.split(" ");

            let command = message[0].toLowerCase();
            let newCommand = message[1];
            let response = message.slice(2);

            if (command === '!lisääkomento') {
                if (message.length < 3) return;
                response = response.join(" ");
                eventEmitter.emit('command-add', { command: newCommand, response: response, channel: data.target });
            }

            if (command === '!poistakomento') {
                if (message.length === 2)
                    eventEmitter.emit('command-remove', { command: newCommand, channel: data.target });
            }

            if (command === '!komennot') {
                this.handleCommands(data);
            }
        });
    }
    async handleCommands(data) {
        let commands = await json.load(['config', 'currentCommands.json']);
        let reservedCommands = await json.load(['config', 'reservedCommands.json']);

        let allCommands = '';

        for (let c of commands) {
            allCommands += `${c.command}, `;
        }

        for (let rc of reservedCommands.commands) {
            allCommands += `${rc}, `;
        }

        allCommands = allCommands.slice(0, -2);

        let msg = new Message(data.target, 'me', 'my@address', `Seuraavat komennot ovat käytössä: ${allCommands}.`);
        eventEmitter.emit('irc-message-send', msg);
    }
}