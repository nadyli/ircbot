// Responsibility: launch command services

const DetectReservedCommand = require('./DetectReservedCommand');
const ParseCommandService = require('./ParseCommandService');
const RegisterCommandService = require('./RegisterCommandService');

module.exports = class CommandService {
    constructor() {
        new DetectReservedCommand();
        new RegisterCommandService();
        new ParseCommandService();
    }
}