const eventEmitter = require('../basic/EventService').getEmitter();
const json = require('../utility/json');
const Message = require('../../models/message');
const fetch = require('node-fetch');

// New API endpoint for Kotipizza
const kotipizzaURL = "https://apim-kotipizza-ecom-prod.azure-api.net/shops/restaurants/delivering-to-point";

module.exports = class KotipizzaCommandService {
    constructor() {
        eventEmitter.on('reserved-command', data => {
            this.handleCommands(data);
        });
    }

    async handleCommands(data) {
        let message = data.message.split(" ");
        let command = message[0].toLowerCase();

        if (command === '!kotipizza') {
            let content = await json.load(['config', 'kotipizzaLocations.json']);
            let locations = content.locations;

            let prices = [];
            for (let location of locations) {
                let price = await this.checkPrices(location);
                if (price) {
                    prices.push(price);
                }
            }

            if (prices.length > 0) {
                let formattedDeliveryPrices = "";
                prices.forEach(function (price) {
                    if (price.dynamicPrice) {
                        formattedDeliveryPrices += `${price.city}: ${price.dynamicPrice}e `;
                    } else {
                        formattedDeliveryPrices += `${price.city}: ${price.staticPrice}eˢ `;
                    }
                });
                let msg = new Message(
                    data.target,
                    'me',
                    'my@address',
                    `Kotipizzan kotiinkuljetuksen hinnat: ${formattedDeliveryPrices}`
                );
                eventEmitter.emit('irc-message-send', msg);
            } else {
                let msg = new Message(
                    data.target,
                    'me',
                    'my@address',
                    `Jokin meni pieleen. Ravintolat lienevät kiinni.`
                );
                eventEmitter.emit('irc-message-send', msg);
            }
        }
    }

    async checkPrices(location) {
        if (!location.hasOwnProperty('lat') || !location.hasOwnProperty('lon')) {
            console.log("Kotipizza has been improperly configured");
            return;
        }

        let response = await fetch(kotipizzaURL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ lat: location.lat, lng: location.lon })
        });

        if (response.status !== 200) {
            console.log("Error fetching Kotipizza dynamic prices");
            return;
        }
        let jsonResponse = await response.json();

        // Assuming the response structure remains similar to the previous API
        if (jsonResponse.length > 0 && jsonResponse[0].dynamicDeliveryFee) {
            return {
                "city": jsonResponse[0].city,
                "location": jsonResponse[0].displayName.split(" ")[0].replace(/[^0-9a-zöä]/gi, ''),
                "dynamicPrice": jsonResponse[0].dynamicDeliveryFee
            };
        }
        if (jsonResponse.length > 0 && jsonResponse[0].deliveryFee) {
            return {
                "city": jsonResponse[0].city,
                "location": jsonResponse[0].displayName.split(" ")[0].replace(/[^0-9a-zöä]/gi, ''),
                "staticPrice": jsonResponse[0].deliveryFee
            };
        }
        return;
    }
}

