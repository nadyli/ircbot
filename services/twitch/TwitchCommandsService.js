// Responsibility: handle Twitch Service specific commands
const eventEmitter = require('../basic/EventService').getEmitter();
const json = require('../utility/json');
const TwitchAPI = require('./TwitchAPI');
const Message = require('../../models/message');

module.exports = class TwitchCommandService {
    constructor() {
        eventEmitter.on('reserved-command', data => {
            this.handleCommands(data);
        });
    }

    async handleCommands(data) {
        let message = data.message.split(" ");
        let command = message[0].toLowerCase();

        if (command === '!live') {
            let content = await json.load(['config', 'followedStreams.json']);
            let streams = content.streams;

            let onlineStreams = 0;

            let msg = new Message(data.target, 'me', 'my@address', `Tarkistetaan onko striimeja online...`);
            eventEmitter.emit('irc-message-send', msg);
            for (let stream of streams) {
                let streamData = await TwitchAPI.getStreamDataWithUsername(stream)
                if (streamData === false) continue;
                let msg = new Message(data.target, 'me', 'my@address', `${stream} on online ja pelaa peliä ${streamData.game_name} otsikolla ${streamData.title}! http://twitch.tv/${stream}`);
                eventEmitter.emit('irc-message-send', msg);
                onlineStreams++;
            }
            if (onlineStreams == 0) {
                let msg = new Message(data.target, 'me', 'my@address', `Ei striimejä online. :(`);
                eventEmitter.emit('irc-message-send', msg);
            }
        }
        if (command === '!striimit') {
            let content = await json.load(['config', 'followedStreams.json']);
            let streams = content.streams.join(", ");
            let msg = new Message(data.target, 'me', 'my@address', `Seuraavat striimit ovat seurannassa: ${streams}`);
            eventEmitter.emit('irc-message-send', msg);
        }
        if (command === '!lisää') {
            if (message.length < 2) return;
            let streams = message.splice(1);
            for (let stream of streams) {
                let info = { stream: stream, channel: data.target };
                eventEmitter.emit('stream-add', info);
            }
        }
        if (command === '!poista') {
            if (message.length < 2) return;
            let streams = message.splice(1);
            for (let stream of streams) {
                let info = { stream: stream, channel: data.target };
                eventEmitter.emit('stream-remove', info);
            }
        }
    }
}