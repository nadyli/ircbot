const fetch = require('node-fetch');
const eventEmitter = require('../basic/EventService').getEmitter();

const OAuth2TokenRequestURI = process.env.NODE_ENV === "production" ? "https://id.twitch.tv/oauth2/token" : "http://localhost:8080/auth/token";
const UserAuthTokenRequestURI = process.env.NODE_ENV === "production" ? "" : "http://localhost:8080/auth/authorize";
const UserRequestURI = process.env.NODE_ENV === "production" ? "https://api.twitch.tv/helix/users?login=" : "http://localhost:8080/units/users?login=";
const ClipRequestURI = process.env.NODE_ENV === "production" ? "https://api.twitch.tv/helix/clips?id=" : "http://localhost:8080/units/clips?id=";
const StreamRequestURI = process.env.NODE_ENV === "production" ? "https://api.twitch.tv/helix/streams?user_id=" : "http://localhost:8080/units/streams?user_id=";
const EventSubURI = process.env.NODE_ENV === "production" ? "https://api.twitch.tv/helix/eventsub/subscriptions" : "localhost:8080/eventsub/subscriptions";
const MockCLIClientURI = "http://localhost:8080/units/clients";

let headers = "";
let access_token = "";
let expiryDate = "";

let user_id = 46282390;

// Get Twitch stream data for specified username
// Example Data
/*
{
  id: '45412760267',
  user_id: '26154978',
  user_login: 'xxx',
  game_id: '520083701',
  game_name: 'RISK: The Game of Global Domination',
  type: 'live',
  title: 'AMONG US RISK AND THEN VARIETY',
  viewer_count: 4212,
  started_at: '2025-01-09T21:48:09Z',
  language: 'en',
  thumbnail_url: 'https://static-cdn.jtvnw.net/previews-ttv/live_user_sykkuno-{width}x{height}.jpg',
  tag_ids: [],
  tags: [ 'English' ],
  is_mature: false
}
*/

exports.getStreamDataWithUsername = async (username) => {
    eventEmitter.emit('log', `Starting a request for Twitch Stream Data with Username: ${username}.`);
    if (!await checkIfTokenIsValid()) {
        let success = await requestOAuthToken();
        if (success === false) {
            eventEmitter.emit('log-error', "Could not get OAuth token, aborting early.");
            return false;
        }
    }
    let data = await requestStreamDataWithUsername(username);

    if (data === undefined || data == null || data == false) {
        eventEmitter.emit('log-error', "Error getting Twitch Stream online status. Data is undefined.");
        return false;
    }

    return data === false ? false : data;
}

exports.getStreamDataWithURL = async (url) => {
    eventEmitter.emit('log', "Starting a request for Twitch Stream Data with url.");
    let username = await parseUsernameOrClipHashFromURL(url);

    if (!await checkIfTokenIsValid()) {
        let success = await requestOAuthToken();
        if (success === false) {
            eventEmitter.emit('log-error', "Could not get OAuth token, aborting early.");
            return false;
        }
    }

    let data = await requestStreamDataWithUsername(username);

    if (data === undefined || data == null || data == false) {
        eventEmitter.emit('log-error', "Error getting Twitch Stream online status. Data is undefined.");
        return false;
    }

    return data === false ? false : data;
}

// Get title of a Twitch stream clip
/*  Example Data
{
  id: 'ColorfulHumbleOrangeRlyTho-kRQGDf7yBqT-U31T',
  url: 'https://clips.twitch.tv/ColorfulHumbleOrangeRlyTho-kRQGDf7yBqT-U31T',
  embed_url: 'https://clips.twitch.tv/embed?clip=ColorfulHumbleOrangeRlyTho-kRQGDf7yBqT-U31T',
  broadcaster_id: '51496027',
  broadcaster_name: 'loltyler1',
  creator_id: '779750662',
  creator_name: 'NOXCIIl',
  video_id: '2347066752',
  game_id: '18122',
  language: 'en',
  title: 'gkick Sequisha',
  view_count: 124450,
  created_at: '2025-01-07T21:01:24Z',
  thumbnail_url: 'https://static-cdn.jtvnw.net/twitch-clips-thumbnails-prod/ColorfulHumbleOrangeRlyTho-kRQGDf7yBqT-U31T/8e89edfc-e060-46c1-b571-a5afded739c5/preview-480x272.jpg',
  duration: 60,
  vod_offset: 1034,
  is_featured: false
}
*/
exports.getClipTitle = async (url) => {
    eventEmitter.emit('log', "Starting a request for Twitch Clip Title with url.");

    if (!await checkIfTokenIsValid()) {
        let success = await requestOAuthToken();
        if (success === false) {
            eventEmitter.emit('log-error', "Could not get OAuth token, aborting early.");
            return false;
        }
    }

    let clipHash = await parseUsernameOrClipHashFromURL(url);
    let clipData = await requestClipDataWithClipHash(clipHash);

    if (clipData === undefined || clipData == null || clipData == false) {
        eventEmitter.emit('log-error', "Error getting Twitch Stream online status. Data is undefined.");
        return false;
    }

    return clipData;
}

async function checkIfTokenIsValid() {
    if (access_token === "" || expiryDate === "") return false;
    if (Date.now() <= expiryDate) return false;
    return true;
}

// Parse Twitch username from URL
async function parseUsernameOrClipHashFromURL(url) {
    let lastIndexOfSlash = url.lastIndexOf("/");
    let id = url.slice(lastIndexOfSlash + 1, url.length);
    id = id.replace("?s=20", "");
    id = id.replace("?filter=clips", "");
    id = id.replace("?sort=time", "");
    id = id.replace("?referrer=raid", "");
    return id;
}

exports.requestUserToken = async (userInfo) => {
    let token = await requestOAuthToken(true, userInfo);
    return token;
}

async function requestOAuthToken(PubSubToken = false, userInfo = {}) {
    let response = {};
    try {
        eventEmitter.emit('log', "Fetching Twitch OAuth token.");
        if (PubSubToken === false) {
            response = await fetch(`${OAuth2TokenRequestURI}?client_id=${process.env.TWITCH_CLIENT_ID}&client_secret=${process.env.TWITCH_CLIENT_SECRET}&grant_type=client_credentials`, { method: 'post' });
        } else {
            response = await fetch(`${UserAuthTokenRequestURI}?client_id=${userInfo.ID}&client_secret=${userInfo.Secret}&grant_type=user_token&user_id=${user_id}&scope=`, { method: "post" });
        }
    } catch (error) {
        eventEmitter.emit('log-error', "Error getting Twitch OAuth token. Fetch failed.");
        // Printing error will share client ID and secret to logs. Do not print. 
        //eventEmitter.emit('log-error', error);
        return false;
    }

    if (!response.ok) {
        eventEmitter.emit('log-error', "Error getting Twitch OAuth token. HTTP response status was not in normal acceptable change.");
        return false;
    }

    response = await response.json();

    if (PubSubToken === false) {
        expiryDate = Date.now() + response.expires_in;
        access_token = response.access_token;
        await updateHeaders();
    } else {
        return response;
    }

    eventEmitter.emit('log', "Twitch OAuth token fetched succesfully.");
}

// Local Twitch CLI must be running
exports.requestMockClientInfo = async () => {
    eventEmitter.emit('log', "Starting a request for Twitch Client Info.");
    let response = ""
    try {
        response = await fetch(MockCLIClientURI);
    } catch (error) {
        eventEmitter.emit('log', "Failed to get Twitch Client Info.");
        return false;
    }
    eventEmitter.emit('log', "Succesfully got Twitch Client Info.");
    response = await response.json();
    return {
        "ID": response.data[0].ID,
        "Secret": response.data[0].Secret
    }
}

async function updateHeaders() {
    headers = {
        'Client-ID': process.env.TWITCH_CLIENT_ID,
        'Authorization': 'Bearer ' + access_token
    }
}

// Get Twitch UserID with Twitch username
async function requestUserIdWithUsername(username) {
    let response = {};
    try {
        eventEmitter.emit('log', `Fetching Twitch UserId with Username: ${username}.`);
        response = await fetch(`${UserRequestURI}${username}`, { headers: headers });
    } catch (error) {
        eventEmitter.emit('log-error', "Error getting Twitch UserId with Username. Fetch failed.");
        eventEmitter.emit('log-error', error);
        return false;
    }

    if (!response.ok) {
        eventEmitter.emit('log-error', "Error getting Twitch UserId with Username. HTTP response status was not in normal acceptable change.");
        return false;
    }

    response = await response.json();

    eventEmitter.emit('log', "Twitch UserId fetched sucecsfully.");
    return response.data[0].id;
}

async function requestClipDataWithClipHash(ClipHash) {
    let response;

    try {
        eventEmitter.emit('log', `Fetching Twitch Clip title with ClipHash ${ClipHash}.`);
        response = await fetch(`${ClipRequestURI}${ClipHash}`, { headers: headers });
    } catch (error) {
        eventEmitter.emit('log-error', "Error getting Twitch Clip title with ClipHash. Fetch failed.");
        eventEmitter.emit('log-error', error);
        return false;
    }

    if (!response.ok) {
        eventEmitter.emit('log-error', "Error getting Twitch Clip title with ClipHash. HTTP response status was not in normal acceptable change.");
        return false;
    }

    response = await response.json();

    if (response.data.length === 0) {
        eventEmitter.emit('log', "Twitch Clip not found.");
        return false;
    }

    eventEmitter.emit('log', "Twitch Clip title with ClipHash fetched succesfully.");

    return response.data[0];
}

// Info about the current state of stream, for example what game is being played
// Return false is specified stream is not currently online.
// e.g. ${game_name}
async function requestStreamDataWithUsername(username) {
    let response = {};
    try {
        eventEmitter.emit('log', `Fetching Twitch Stream Data with Username: ${username}.`);
        let id = await requestUserIdWithUsername(username);
        response = await fetch(`${StreamRequestURI}${id}`, { headers: headers });
    } catch (error) {
        eventEmitter.emit('log-error', "Error getting Twitch Stream Data with Username. Fetch failed.");
        eventEmitter.emit('log-error', error);
        return false;
    }

    if (!response.ok) {
        eventEmitter.emit('log-error', "Error getting Twitch Stream Data with Username. HTTP response status was not in normal acceptable change.");
        return false;
    }

    eventEmitter.emit('log', "Twitch Stream Data with Username fetched succesfully.");
    response = await response.json();

    if (response.data.length === 0) {
        eventEmitter.emit('log', "Twitch User is not online.");
        return false;
    }

    return response.data[0];
}