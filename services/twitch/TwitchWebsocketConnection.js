// Responsibility: handle twitch realtime websocket functionatility

const eventEmitter = require('../basic/EventService').getEmitter();
const WebSocketClient = require('websocket').client;

module.exports = class TwitchWebSocketConnection {
    constructor(serverURL = "ws://127.0.0.1:8080/ws") {
        this.client = new WebSocketClient();
        this.timer = null;
        this.timerTimeInSeconds = 10 * 1000;
        this.timerOffsetInSeconds = 30 * 1000;

        this.client.on('connect', connection => {
            eventEmitter.emit('log', "Connected to Twitch WebSocket succesfully.");
            let id = "";

            connection.on('message', data => {
                // Parse data
                let utf8Data = data.utf8Data;
                let objectData = JSON.parse(utf8Data);
                let payload = objectData.payload;
                let payload_session = payload.session;
                let message_type = objectData.metadata.message_type;

                // Welcome message
                if (message_type == 'session_welcome') {
                    444
                    eventEmitter.emit('log', "Recieved welcome message from Twitch WebSocket.");

                    id = payload_session.id;
                    this.timerTimeInSeconds = payload_session.keepalive_timeout_seconds * 1000;

                    eventEmitter.emit('log', `Got the id: ${id}`);
                    eventEmitter.emit("twitch", id);
                }

                // Notification event, subscription
                if (message_type == 'notification') {
                    eventEmitter.emit('log', "Recieved notification from Twitch WebSocket.");
                    let username = payload.event.broadcaster_user_name;
                    console.log(`${username} is now online! http://www.twitch.tv/${username}`);
                    timer.refresh();
                }

                if (message_type == 'session_reconnect') {
                    // The server will close in 30 seconds. Reconnect to the specified url.
                    eventEmitter.emit('log', "Recieved session reconnection event from Twitch WebSocket.");

                    new TwitchWebSocketConnection(payload_session.reconnect_url);

                    eventEmitter.on('log', async message => {
                        if (message.includes("Recieved welcome message from Twitch WebSocket.")) {
                            connection.drop(1001);
                            clearTimeout(this.timer);
                        }
                    });
                }
                // Something happened to our subscription
                if (message_type == 'revocation') {
                    eventEmitter.emit('log', "Recieved revocation event from Twitch WebSocket.");
                    let metadata = objectData.metadata;
                    let status = metadata.subscription.status;
                    switch (status) {
                        case 'user_removed':
                            eventEmitter.emit('log', `Twitch WebSocket: Subscribed user with id ${metadata.subscription.condition.broadcaster_user_id} removed.`);
                            break;
                        case 'authorization_revoked':
                            eventEmitter.emit('log', `Twitch WebSocket: The user revoked the authorization token that the subscription relied on.`);
                            break;
                        case 'version_removed':
                            eventEmitter.emit('log', `Twitch WebSocket: The subscribed to subscription type and version is no longer supported`);
                            break;
                    }
                }

                // Keep the session alive
                if (message_type == 'session_keepalive') {
                    eventEmitter.emit('log', "Recieved keepalive message from Twitch WebSocket.");
                    this.resetTimer();
                }
            });

            connection.on('error', error => {
                eventEmitter.emit('log-error', `Connection error in Twitch WebSocket. Error: ${error}.`);
                // Close event will automatically be called
            });
            connection.on('close', () => {
                eventEmitter.emit('log', "Connection to Twitch WebSocket closed. Reconnecting in 10 seconds.");
                setTimeout(function () { new TwitchWebSocketConnection(); }, this.timerTimeInSeconds);
                clearTimeout(this.timer);
            });
        });

        this.client.on('connectFailed', error => {
            eventEmitter.emit('log-error', `Failure to connect to Twitch WebSocket: ${error}. Reconnecting in 10 seconds.`);
            setTimeout(function () { new TwitchWebSocketConnection(); }, this.timerTimeInSeconds);
        });

        this.client.connect(serverURL);
    }

    async resetTimer() {
        if (this.timer != null) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
            new TwitchWebSocketConnection();
        }, (this.timerTimeInSeconds + this.timerOffsetInSeconds));
    }
}