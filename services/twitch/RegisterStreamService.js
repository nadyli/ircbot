// Responsibility: register and unregister twitch streams
const eventEmitter = require('../basic/EventService').getEmitter();
const json = require('../utility/json');
const Message = require('../../models/message');

module.exports = class RegisterStreamService {
    constructor() {
        eventEmitter.on('stream-add', data => {
            this.registerStream(data);
        });

        eventEmitter.on('stream-remove', data => {
            this.removeStream(data);
        });
    }

    async registerStream(data) {
        let exists = await this.checkIfStreamExists(data.stream);
        if (exists) {
            let msg = new Message(data.channel, 'me', 'my@address', `${data.stream} on jo seurannassa!`);
            eventEmitter.emit('irc-message-send', msg);
            return;
        } else {
            let content = await json.load(['config', 'followedStreams.json']);
            let streams = content.streams;
            streams.push(data.stream);
            await json.save(['config', 'followedStreams.json'], content);
            let msg = new Message(data.channel, 'me', 'my@address', `${data.stream} on lisätty seurantaan!`);
            eventEmitter.emit('irc-message-send', msg);
        }
    }

    async removeStream(data) {
        let exists = await this.checkIfStreamExists(data.stream);
        if (!exists) {
            let msg = new Message(data.channel, 'me', 'my@address', `${data.stream} ei ole seurannassa!`);
            eventEmitter.emit('irc-message-send', msg);
            return;
        } else {
            let content = await json.load(['config', 'followedStreams.json']);
            let streams = content.streams;

            let index = streams.findIndex(stream => stream == data.stream);
            if (index === -1) return;
            streams.splice(index, 1);

            await json.save(['config', 'followedStreams.json'], content);

            let msg = new Message(data.channel, 'me', 'my@address', `${data.stream} poistettu onnistuneesti seurannasta.`);
            eventEmitter.emit('irc-message-send', msg);
        }
    }

    async checkIfStreamExists(username) {
        let content = await json.load(['config', 'followedStreams.json']);
        let streams = content.streams;
        for (const s of streams) {
            if (username === s) return true;
        }
        return false;
    }
}