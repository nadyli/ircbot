// Responsibility: handle twitch functionatility
const RegisterStreamService = require('./RegisterStreamService');
const TwitchCommandsService = require('./TwitchCommandsService');

module.exports = class TwitchService {
    constructor() {
        new RegisterStreamService();
        new TwitchCommandsService();
    }
}