// Responsibility: handle Stock Service specific commands
const eventEmitter = require('../basic/EventService').getEmitter();
const json = require('../utility/json');
const Message = require('../../models/message');
const fetch = require('node-fetch');

const abbreviations = {
  hki: 'Helsinki',
  jns: 'Joensuu',
  kpo: 'Kuopio',
  tre: 'Tampere',
  sj: 'Siilinjärvi'
}

module.exports = class WeatherCommandService {
  constructor() {
    eventEmitter.on('reserved-command', data => {
      this.handleCommands(data);
    });
  }

  formIrcMessage(data, msg) {
    return new Message(data.target, 'me', 'my@address', msg);
  }

  async handleCommands(data) {
    let splitMsg = data?.message?.split(" ");
    let [command, ...location] = splitMsg;
    command = command.toLowerCase();
    let city = location.join(" ");

    let msg = '';
    if (command !== '!sää') return null;
    if (city.length > 0) {
      if (abbreviations[city]) city = abbreviations[city]
      const url = `https://api.weatherapi.com/v1/current.json?lang=fi&key=${process.env.WEATHER_API_KEY}&q=${city}&aqi=no`
      const result = await fetch(url);
      const { location, current } = await result?.json();
      if (location, current) {
        const message = `${location?.name}: lämpötila: ${current?.temp_c}°C, tuntuu kuin: ${current?.feelslike_c}°C. Suhteellinen kosteus: ${current?.humidity}%. Säätila: ${current?.condition?.text}`
        msg = this.formIrcMessage(data, message);
      } else {
        const message = `Paikkakunnalla ${city} ei löytynyt säätietoja`
        msg = this.formIrcMessage(data, message);
      }
    } else {
      const message = `Paikkakuntaa ei annettu. Anna paikkakunta muodossa: !sää Kuopio`
      msg = this.formIrcMessage(data, message);
    }
    eventEmitter.emit('irc-message-send', msg);
  }
}
