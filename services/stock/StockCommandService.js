// Responsibility: handle Stock Service specific commands
const eventEmitter = require('../basic/EventService').getEmitter();
const json = require('../utility/json');
const Message = require('../../models/message');
const fetch = require('node-fetch');

module.exports = class StockCommandService {
  constructor() {
    eventEmitter.on('reserved-command', data => {
      this.handleCommands(data);
    });
  }

  formIrcMessage(data, msg) {
    return new Message(data.target, 'me', 'my@address', msg);
  }

  async handleCommands(data) {
    const splittedMessage = data?.message?.split(" ");
    const command = splittedMessage[0].toLowerCase();

    if (command !== '!osake') return null;
    let msg = '';
    if (splittedMessage[1]) {
      const company = splittedMessage[1].toUpperCase();
      const url = `https://query1.finance.yahoo.com/v8/finance/chart/${company}?region=EU&lang=en-US`;
      const response = await fetch(url);
      const jsonData = await response.json();

      if (jsonData?.chart?.result) {
        const { regularMarketPrice, previousClose, symbol, currency } = jsonData.chart?.result[0]?.meta;
        if (regularMarketPrice && previousClose && symbol && currency) {
          const comparedPrice = (1 - (previousClose / regularMarketPrice)) * 100
          const pricePrefix = comparedPrice > 0 ? '+' : '';
          const message = `Osakkeen ${symbol} tämänhetkinen kurssi: ${regularMarketPrice} ${currency}. Muutos ${pricePrefix}${comparedPrice.toFixed(2)}%`;
          msg = this.formIrcMessage(data, message);
        } else {
          const message = `Yritys ${company} löytyi, mutta kaikkia tietoja ei pystytty hakemaan`;
          msg = this.formIrcMessage(data, message);
        }
      } else {
        const message = `Yrityksen ${company} tietoja ei löytynyt`;
        msg = this.formIrcMessage(data, message);
      }
    } else {
      const message = 'Yritystieto puuttuu. Syötä komento muodossa: !osake nokia.he';
      msg = this.formIrcMessage(data, message);
    }
    eventEmitter.emit('irc-message-send', msg);
  }
}