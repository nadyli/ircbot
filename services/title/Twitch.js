const eventEmitter = require('../basic/EventService').getEmitter();
const Message = require('../../models/message');
const TwitchAPI = require('../twitch/TwitchAPI');

module.exports.title = async data => {
    let streamData = await TwitchAPI.getStreamDataWithURL(data.url.original);
    if (!streamData === false) {
        let msg = new Message(data.target, 'me', 'my@address', `${streamData.title} pelissä ${streamData.game_name}`);
        eventEmitter.emit('irc-message-send', msg);
    }
}   