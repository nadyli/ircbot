const YouTube = require('simple-youtube-api');
const youtube = new YouTube(process.env.YOUTUBE_API_KEY)
const eventEmitter = require('../basic/EventService').getEmitter();
const Message = require('../../models/message');

module.exports.title = async data => {
    let video = await youtube.getVideo(data.url.original);
    let msg = new Message(data.target, 'me', 'my@address', `${video.title} kanavalta ${video.channel.title}`);
    eventEmitter.emit('irc-message-send', msg);
}