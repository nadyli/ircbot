// Responsibility: handle title services

const DelegateTitleService = require('./DelegateTitleService');
const ParseForWebsiteService = require('./ParseForWebsiteService');

module.exports = class TitleService {
    constructor() {
        new DelegateTitleService();
        new ParseForWebsiteService();
    }
}