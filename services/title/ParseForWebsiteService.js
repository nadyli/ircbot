// Responsibility: parse message for a website
const eventEmitter = require('../basic/EventService').getEmitter();
const Url = require('../../models/url');

module.exports = class ParseForWebsiteService {
    constructor() {
        eventEmitter.on('parsed-message', message => {
            this.parseForWebsite(message);
        });
    }
    async parseForWebsite(message) {
        if (message.message.includes("http") || message.message.includes("www")) {
            if (message.message.includes(" ")) {
                let websites = message.message.split(" ").filter(element => element.includes("http") || element.includes("www"));
                for (let website of websites) {
                    let url = new Url(website);
                    await this.emitWebsites(url, message.target);
                }
            } else {
                let url = new Url(message.message);
                this.emitWebsites(url, message.target);
            }
        }
    }
    async emitWebsites(url, channel) {
        let data = { url, 'target': channel };
        eventEmitter.emit('website-found', data);
    }
}