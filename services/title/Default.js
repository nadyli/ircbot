const fetch = require('node-fetch');
const eventEmitter = require('../basic/EventService').getEmitter();
const Message = require('../../models/message');
const decode = require('html-entities').decode;

const opt = {
    headers: {
        "Accept-Language": "en-US",
        "User-Agent": "juhan-botti"
    }
}

module.exports.title = async data => {
    let response = await fetch(data.url.original, opt);
    let body = await response.text();
    let firstIndexTitle = body.indexOf("<title");
    let lastIndexTitle = body.indexOf("</title>");
    let title = body.slice(firstIndexTitle, lastIndexTitle);
    let secondIndexTitle = title.indexOf(">");
    title = title.slice(secondIndexTitle + 1);
    title = decode(title);
    let msg = new Message(data.target, 'me', 'my@address', title);
    eventEmitter.emit('irc-message-send', msg);
}