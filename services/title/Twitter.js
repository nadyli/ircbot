const fetch = require('node-fetch');
const eventEmitter = require('../basic/EventService').getEmitter();
const Message = require('../../models/message');

let bearer_token = "";

module.exports.title = async data => {
    // Get token
    if (bearer_token === "") await getBearerToken();

    // Parse ID from URL
    let url = `${data.url.scheme}://${data.url.domainName}${data.url.pathToFile}`;
    let id = await getIdFromUrl(url)

    // Get tweet
    let tweetText = await getTweetTextWithId(id);

    // Parse the tweet text
    let parsedText = await parseTweetText(tweetText);

    // Construct and send message to IRC
    let msg = new Message(data.target, 'me', 'my@address', parsedText);

    console.log(parsedText)
    console.log(msg)
    eventEmitter.emit('irc-message-send', msg);
}

async function getBearerToken() {
    // OAuth endpoint
    let requestTokenURL = "https://api.twitter.com/oauth2/token";

    // Construct base64 bearer token credentials
    let bearer_token_cred = process.env.TWITTER_API_KEY + ":" + process.env.TWITTER_SECRET_KEY;
    let buff = Buffer.from(bearer_token_cred);
    let base64_bearer_token_cred = buff.toString('base64');

    // Request bearer token
    let response = await fetch(requestTokenURL, {
        method: 'post',
        body: "grant_type=client_credentials",
        headers: {
            'Authorization': `Basic ${base64_bearer_token_cred}`,
            'Content-Type': `application/x-www-form-urlencoded;charset=UTF-8`
        }
    });

    // Negative cases
    if (!response.body) {
        console.log("Error getting twitter request token. Couldn't get response body.");
        return;
    }
    if (response.status !== 200) {
        console.log(`Error getting Twitter request token. Error code: ${response.status} Error info: ${response.statusText}`);
        return;
    }

    // Positive case
    response = await response.json();
    bearer_token = response.access_token;

    return;
}

async function getTweetTextWithId(id) {
    // Endpoint for tweets
    let requestTweetURL = "https://api.twitter.com/2/tweets/";

    // Request for tweet
    let response = await fetch(requestTweetURL + id, {
        headers: {
            'Authorization': `Bearer ${bearer_token}`
        }
    });

    // Negative cases
    if (!response.body) {
        console.log("Error getting tweet with ID. Couldn't get response body.");
        return;
    }
    if (response.status !== 200) {
        console.log(`Error getting tweet with ID. Error code: ${response.status} Error info: ${response.statusText}`);
        return;
    }

    // Positive case
    response = await response.json();
    return response.data.text;
}

async function getIdFromUrl(url) {
    // Find the last back slash
    let lastIndexOfSlash = url.lastIndexOf("/");
    // Slice the ID from url
    let id = url.slice(lastIndexOfSlash + 1, url.length);
    // Return ID
    return id;
}

async function parseTweetText(text) {
    // Replace new line with white space
    let parsed = text.replace(/\n/g, " ");
    // Replace 2 or more white spaces with just one
    parsed = parsed.replace(/\s{2,}/g, " ");
    return parsed;
}