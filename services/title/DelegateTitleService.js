// Responsibility: delegate title
const _default = require('./Default');
const twitch = require('./Twitch');
const youtube = require('./YouTube');
const twitter = require('./Twitter');
const TwitchAPI = require('../twitch/TwitchAPI');

const eventEmitter = require('../basic/EventService').getEmitter();
const Message = require('../../models/message');

module.exports = class DelegateTitleService {
    constructor() {
        eventEmitter.on('website-found', data => {
            let url = data.url.original;
            let domainName = data.url.domainName;
            switch (domainName) {
                case ("youtube.com"):
                    youtube.title(data);
                    break;
                case ("clips.twitch.tv"):
                    this.handleTwitchClip(url, data.target);
                    console.log("haloo")
                    break;
                case ("twitch.tv"):
                    this.handleTwitchStream(url, data.target, data.url.pathToFile);
                    break;
                // Broken implementations
                case ("x.com"):
                    eventEmitter.emit('log', "Fetching X.com Title currently broken.");
                    break;
                case ("twitter.com"):
                    eventEmitter.emit('log', "Fetching Twitter.com Title currently broken.");
                    // twitter.title(data);
                    //(data.url.domainName.includes("twitter.com") && data.url.domainName.split(".").length > 2) _default.title(data);
                    break;
                case ("reddit.com"):
                    eventEmitter.emit('log', "Fetching Reddit.com Title currently broken.");
                    break;
                default:
                    _default.title(data);
                    break;
            }
        });
    }

    async handleTwitchStream(streamURL, target, streamName) {
        let streamData = await TwitchAPI.getStreamDataWithURL(streamURL);
        if (streamData == false) return;
        let streamer = streamName.slice(1, streamName.length);
        let msg = new Message(target, 'me', 'my@address', `${streamer} on online ja pelaa peliä ${streamData.game_name} otsikolla ${streamData.title}`);
        eventEmitter.emit('irc-message-send', msg);
    }

    async handleTwitchClip(clipURL, target) {
        let clipData = await TwitchAPI.getClipTitle(clipURL);
        if (clipData == false) return;
        let msg = new Message(target, 'me', 'my@address', `Klippi otsikolla ${clipData.title} kanavalta ${clipData.broadcaster_name}`);
        eventEmitter.emit('irc-message-send', msg);
    }
}