// PathArray example = ['config', 'currentConfiguration.json]

const fs = require('fs');
const path = require('path');
const eventEmitter = require('../basic/EventService.js').getEmitter();

async function loadFileData(pathArray, critical = false) {
    // Figure out relative path
    let paths = [path.dirname(require.main.filename)];
    paths = paths.concat(pathArray);
    let target = path.join.apply(null, paths);
    eventEmitter.emit('log', `Trying to load data from file: ${target}.`);

    try {
        // Loading file
        let data = fs.readFileSync(target);
        // No data found
        if (data.length === 0) return null;
        // Parse data from string
        data = JSON.parse(data);
        eventEmitter.emit('log', `Data loaded succesfully from file: ${target}.`);
        return data;
    } catch (error) {
        eventEmitter.emit('log-error', `Problem loading data from ${target}`);
        if (critical) eventEmitter.emit('app-close', { order: "shutdown", message: "json.js: Could not load a critical file. Sending shutdown order." });
        return false;
    }
}

async function saveFileData(pathArray, data) {
    // Figure out relative path
    let paths = [path.dirname(require.main.filename)];
    paths = paths.concat(pathArray);
    let target = path.join.apply(null, paths);
    eventEmitter.emit('log', `Trying to save data to file: ${target}.`);

    // Check if the folders exists, otherwise create them
    let currentPath = "./";
    if (pathArray.length > 1) {
        for (let i = 1; i < pathArray.length; i++) {
            currentPath += `/${pathArray[i - 1]}`;
            if (!fs.existsSync(currentPath)) fs.mkdirSync(currentPath);
        }
    }

    try {
        // Stringifying the data so we can save it easily
        data = JSON.stringify(data);
        // Write the data
        fs.writeFileSync(target, data, { flag: 'w' });
        eventEmitter.emit('log', `Data saved succesfully to file: ${target}.`);
        return true;
    } catch (error) {
        console.log(`Problem saving data to ${target}.`);
        return false;
    }
}

module.exports.load = loadFileData;
module.exports.save = saveFileData;