module.exports = {
    collectCoverageFrom: ["**/*.js", "!**/node_modules/**"],
    coverageReporters: ["html", "text", "text-summary"],
    testMatch: ["**/*.test.js"]
}