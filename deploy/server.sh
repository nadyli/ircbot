#!/bin/bash

export PATH=$PATH:/home/ircbot/.nvm/versions/node/v22.11.0/bin

cd ircbot
git checkout main
git pull origin main

npm install
pm2 restart IRCBot