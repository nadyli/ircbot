Functionatility is based on emitter. Get emitter from ./services/basic/EventService with function getEmitter.

Logging events: 'log', 'log-warning', 'log-error'
IRC-related events: 'connect, 'ready', 'data', 'error', 'timeout', 'close', 'end'
Bot functionatility events: 'network-data-received' (raw data from connection), 'network-data-send' (raw data to connection), 'network-socket' (socket sent from succesfull connection), 'irc-message-send' (send message to server, send message-model data)
Reserved commands: 'reserved-command' (get message-model data)