module.exports = class Url {
    constructor(url) {
        url = url.trim();

        this.original = url;

        // Scheme
        if (url.includes("http") || url.includes("https")) {
            // Protocol
            let scheme;
            let firstColon = url.indexOf(":");
            scheme = url.slice(0, firstColon);
            this.scheme = scheme;
            // Trim already found information
            url = url.slice(firstColon, url.length);
            url = url.replace("://", "");
        } else {
            this.scheme = "";
        }

        // Domain Name + Port
        let domainName;
        let firstForwardSlash = url.indexOf("/");
        if (firstForwardSlash !== -1) {
            domainName = url.slice(0, firstForwardSlash);
        } else {
            domainName = url;
        }

        // Separate Domain Name and possible port
        if (domainName.includes(":")) {
            let portLocation = url.indexOf(":");
            let port = url.slice(portLocation + 1, domainName.length);
            domainName = url.slice(0, portLocation);
            this.domainName = domainName;
            this.port = port;
            // Trim already found information
            url = url.replace(domainName, "");
            url = url.replace(":" + port, "");
        } else {
            this.domainName = domainName;
            this.port = '';
            // Trim already found information
            url = url.replace(domainName, "");
        }

        // Remove www if present, we already have identified the web address
        if (domainName.includes("www.")) {
            this.domainName = domainName.slice(4, domainName.length);
        }

        // Path to file
        let pathToFile;
        if (url.includes("?")) {
            let parameterIndex = url.indexOf("?");
            pathToFile = url.slice(0, parameterIndex);
            this.pathToFile = pathToFile;
        } else {
            this.pathToFile = url;
        }

        // Trim already found information
        url = url.replace(pathToFile, "");

        // Anchor
        if (url.includes("#")) {
            let hashTagIndex = url.indexOf("#");
            let anchor = url.slice(hashTagIndex, url.length);
            this.anchor = anchor;
            // Trim already found information
            url = url.slice(0, hashTagIndex);
        } else {
            this.anchor = "";
        }

        // Parameters
        if (url.includes("?")) {
            // Remove "?"
            url = url.substring(1);

            let parameters = [];
            let splitParameters = url.split("&");
            splitParameters.forEach(element => {
                let keyValuePair = element.split("=")
                parameters.push({
                    key: keyValuePair[0],
                    value: keyValuePair[1]
                })
            });
            this.parameters = parameters;
        } else {
            this.parameters = '';
        }
    }
}