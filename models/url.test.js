/* Example URLs tested:
127.0.0.1:8080
www.google.com
http://google.com
http://www.google.com
www.iskuri.org/quotes
https://www.youtube.com/watch?v=tAuRQs_d9F8
https://www.rfc-editor.org/rfc/rfc1459#section-1.2
http://www.example.com:80/path/to/myfile.html?key1=value1&key2=value2#SomewhereInTheDocument
https://www.reddit.com/r/GlobalOffensive/comments/11m2j1m/pain_vs_ninjas_in_pyjamas_esl_pro_league_season/
https://www.linkedin.com/jobs/search/?currentJobId=3326269025&distance=25&f_WT=2&geoId=112518871&keywords=it
*/

const Url = require('./url')

describe("Testing parsing URLs", () => {
    describe("Testing 127.0.0.1:8080", () => {
        let testObject = new Url("127.0.0.1:8080");
        test('Original', () => {
            expect(testObject.original).toBe("127.0.0.1:8080");
        });
        test('Scheme', () => {
            expect(testObject.scheme).toBe("");
        });
        test('Domain Name', () => {
            expect(testObject.domainName).toBe("127.0.0.1");
        });
        test('Port', () => {
            expect(testObject.port).toBe("8080");
        });
        test('Path to file', () => {
            expect(testObject.pathToFile).toBe("");
        });
        test('Anchor', () => {
            expect(testObject.anchor).toBe("");
        });
        test('Parameters', () => {
            expect(testObject.parameters).toBe("");
        });
    });

    describe("Testing www.google.com", () => {
        let testObject = new Url("www.google.com");
        test('Original', () => {
            expect(testObject.original).toBe("www.google.com");
        });
        test('Scheme', () => {
            expect(testObject.scheme).toBe("");
        });
        test('Domain Name', () => {
            expect(testObject.domainName).toBe("google.com");
        });
        test('Port', () => {
            expect(testObject.port).toBe("");
        });
        test('Path to file', () => {
            expect(testObject.pathToFile).toBe("");
        });
        test('Anchor', () => {
            expect(testObject.anchor).toBe("");
        });
        test('Parameters', () => {
            expect(testObject.parameters).toBe("");
        });
    });

    describe("Testing http://google.com", () => {
        let testObject = new Url("http://google.com");
        test('Original', () => {
            expect(testObject.original).toBe("http://google.com");
        });
        test('Scheme', () => {
            expect(testObject.scheme).toBe("http");
        });
        test('Domain Name', () => {
            expect(testObject.domainName).toBe("google.com");
        });
        test('Port', () => {
            expect(testObject.port).toBe("");
        });
        test('Path to file', () => {
            expect(testObject.pathToFile).toBe("");
        });
        test('Anchor', () => {
            expect(testObject.anchor).toBe("");
        });
        test('Parameters', () => {
            expect(testObject.parameters).toBe("");
        });
    });

    describe("Testing http://www.google.com", () => {
        let testObject = new Url("http://www.google.com");
        test('Original', () => {
            expect(testObject.original).toBe("http://www.google.com");
        });
        test('Scheme', () => {
            expect(testObject.scheme).toBe("http");
        });
        test('Domain Name', () => {
            expect(testObject.domainName).toBe("google.com");
        });
        test('Port', () => {
            expect(testObject.port).toBe("");
        });
        test('Path to file', () => {
            expect(testObject.pathToFile).toBe("");
        });
        test('Anchor', () => {
            expect(testObject.anchor).toBe("");
        });
        test('Parameters', () => {
            expect(testObject.parameters).toBe("");
        });
    });

    describe("Testing www.iskuri.org/quotes", () => {
        let testObject = new Url("www.iskuri.org/quotes");
        test('Original', () => {
            expect(testObject.original).toBe("www.iskuri.org/quotes");
        });
        test('Scheme', () => {
            expect(testObject.scheme).toBe("");
        });
        test('Domain Name', () => {
            expect(testObject.domainName).toBe("iskuri.org");
        });
        test('Port', () => {
            expect(testObject.port).toBe("");
        });
        test('Path to file', () => {
            expect(testObject.pathToFile).toBe("/quotes");
        });
        test('Anchor', () => {
            expect(testObject.anchor).toBe("");
        });
        test('Parameters', () => {
            expect(testObject.parameters).toBe("");
        });
    });

    describe("Testing https://www.youtube.com/watch?v=tAuRQs_d9F8", () => {
        let testObject = new Url("https://www.youtube.com/watch?v=tAuRQs_d9F8");
        test('Original', () => {
            expect(testObject.original).toBe("https://www.youtube.com/watch?v=tAuRQs_d9F8");
        });
        test('Scheme', () => {
            expect(testObject.scheme).toBe("https");
        });
        test('Domain Name', () => {
            expect(testObject.domainName).toBe("youtube.com");
        });
        test('Port', () => {
            expect(testObject.port).toBe("");
        });
        test('Path to file', () => {
            expect(testObject.pathToFile).toBe("/watch");
        });
        test('Anchor', () => {
            expect(testObject.anchor).toBe("");
        });

        let testParameters = [
            { key: 'v', value: 'tAuRQs_d9F8' }
        ];

        test('Parameters', () => {
            expect(testObject.parameters).toMatchObject(testParameters);
        });
    });

    describe("Testing https://www.rfc-editor.org/rfc/rfc1459#section-1.2", () => {
        let testObject = new Url("https://www.rfc-editor.org/rfc/rfc1459#section-1.2");
        test('Original', () => {
            expect(testObject.original).toBe("https://www.rfc-editor.org/rfc/rfc1459#section-1.2");
        });
        test('Scheme', () => {
            expect(testObject.scheme).toBe("https");
        });
        test('Domain Name', () => {
            expect(testObject.domainName).toBe("rfc-editor.org");
        });
        test('Port', () => {
            expect(testObject.port).toBe("");
        });
        test('Path to file', () => {
            expect(testObject.pathToFile).toBe("/rfc/rfc1459#section-1.2");
        });
        test('Anchor', () => {
            expect(testObject.anchor).toBe("#section-1.2");
        });
        test('Parameters', () => {
            expect(testObject.parameters).toBe("");
        });
    });

    describe("Testing http://www.example.com:80/path/to/myfile.html?key1=value1&key2=value2#SomewhereInTheDocument", () => {
        let testObject = new Url("http://www.example.com:80/path/to/myfile.html?key1=value1&key2=value2#SomewhereInTheDocument");
        test('Original', () => {
            expect(testObject.original).toBe("http://www.example.com:80/path/to/myfile.html?key1=value1&key2=value2#SomewhereInTheDocument");
        });
        test('Scheme', () => {
            expect(testObject.scheme).toBe("http");
        });
        test('Domain Name', () => {
            expect(testObject.domainName).toBe("example.com");
        });
        test('Port', () => {
            expect(testObject.port).toBe("80");
        });
        test('Path to file', () => {
            expect(testObject.pathToFile).toBe("/path/to/myfile.html");
        });
        test('Anchor', () => {
            expect(testObject.anchor).toBe("#SomewhereInTheDocument");
        });

        let testParameters = [
            { key: 'key1', value: 'value1' },
            { key: 'key2', value: 'value2' }
        ];

        test('Parameters', () => {
            expect(testObject.parameters).toMatchObject(testParameters);
        });
    });

    describe("Testing https://www.reddit.com/r/GlobalOffensive/comments/11m2j1m/pain_vs_ninjas_in_pyjamas_esl_pro_league_season/", () => {
        let testObject = new Url("https://www.reddit.com/r/GlobalOffensive/comments/11m2j1m/pain_vs_ninjas_in_pyjamas_esl_pro_league_season/");
        test('Original', () => {
            expect(testObject.original).toBe("https://www.reddit.com/r/GlobalOffensive/comments/11m2j1m/pain_vs_ninjas_in_pyjamas_esl_pro_league_season/");
        });
        test('Scheme', () => {
            expect(testObject.scheme).toBe("https");
        });
        test('Domain Name', () => {
            expect(testObject.domainName).toBe("reddit.com");
        });
        test('Port', () => {
            expect(testObject.port).toBe("");
        });
        test('Path to file', () => {
            expect(testObject.pathToFile).toBe("/r/GlobalOffensive/comments/11m2j1m/pain_vs_ninjas_in_pyjamas_esl_pro_league_season/");
        });
        test('Anchor', () => {
            expect(testObject.anchor).toBe("");
        });
        test('Parameters', () => {
            expect(testObject.parameters).toBe("");
        });
    });

    describe("Testing https://www.linkedin.com/jobs/search/?currentJobId=3326269025&distance=25&f_WT=2&geoId=112518871&keywords=it", () => {
        let testObject = new Url("https://www.linkedin.com/jobs/search/?currentJobId=3326269025&distance=25&f_WT=2&geoId=112518871&keywords=it");
        test('Original', () => {
            expect(testObject.original).toBe("https://www.linkedin.com/jobs/search/?currentJobId=3326269025&distance=25&f_WT=2&geoId=112518871&keywords=it");
        });
        test('Scheme', () => {
            expect(testObject.scheme).toBe("https");
        });
        test('Domain Name', () => {
            expect(testObject.domainName).toBe("linkedin.com");
        });
        test('Port', () => {
            expect(testObject.port).toBe("");
        });
        test('Path to file', () => {
            expect(testObject.pathToFile).toBe("/jobs/search/");
        });
        test('Anchor', () => {
            expect(testObject.anchor).toBe("");
        });

        let testParameters = [
            { key: 'currentJobId', value: '3326269025' },
            { key: 'distance', value: '25' },
            { key: 'f_WT', value: '2' },
            { key: 'geoId', value: '112518871' },
            { key: 'keywords', value: 'it' }
        ];

        test('Parameters', () => {
            expect(testObject.parameters).toMatchObject(testParameters);
        });
    });
});


// Template
// describe("Testing ", () => {
//     let testObject = new Url("");
//     test('Scheme', () => {
//         expect(testObject.scheme).toBe("");
//     });
//     test('Domain Name', () => {
//         expect(testObject.domainName).toBe("");
//     });
//     test('Port', () => {
//         expect(testObject.port).toBe("");
//     });
//     test('Path to file', () => {
//         expect(testObject.pathToFile).toBe("");
//     });
//     test('Anchor', () => {
//         expect(testObject.anchor).toBe("");
//     });
//     test('Parameters', () => {
//         expect(testObject.parameters).toBe("");
//     });
// });
