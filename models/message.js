module.exports = class Message {
    constructor(target, sender, senderAddress, message) {
        let date = new Date(Date.now());
        this.time = `${date.toLocaleString()}`;
        this.target = target;
        this.sender = sender;
        this.senderAddress = senderAddress;
        this.message = message;
    }
}