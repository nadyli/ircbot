require('dotenv').config();

const TwitchService = require('./services/twitch/TwitchService');
const TitleService = require('./services/title/TitleService');
const CommandService = require('./services/commands/CommandService');
const LogService = require('./services/basic/LogService');
const IrcService = require('./services/irc/IrcService');
const KotipizzaService = require('./services/kotipizza/KotipizzaService');
const StockService = require('./services/stock/StockService');
const WeatherService = require('./services/weather/WeatherService');

const eventEmitter = require('./services/basic/EventService').getEmitter();

function main() {
    new LogService();
    new TwitchService();
    if (process.env.TWITCH_CLIENT_ID === undefined || process.env.TWITCH_CLIENT_SECRET === undefined) { shutdown() }
    new TitleService();
    new CommandService();
    if (process.env.NODE_ENV === undefined || process.env.QAUTH === undefined || process.env.QPASSWORD === undefined || process.env.QHIDEHOST === undefined || process.env.DEBUG === undefined) {
        shutdown()
    }
    new IrcService();
    new KotipizzaService();
    new StockService();
    new WeatherService();
}

function shutdown() {
    eventEmitter.emit("log-error", `Enviroment variables are not set correctly. Killing the process.`);
    process.exit(1);
}

eventEmitter.on('app-close', data => {
    if (data.order) {
        eventEmitter.emit("log-error", `Shutdown order recieved. Killing the process. ${data.message}`);
        process.exit(1);
    }
});



process.on('SIGTERM', () => {
    eventEmitter.emit('log', 'Process terminated.');
    console.log('Process terminated.');
});

main();
